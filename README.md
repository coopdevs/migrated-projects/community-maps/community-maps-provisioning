## Setup

### Configure python environment

In order to assure that the playbooks contained here are run always with the same ansible version and linted the same way, we are using [`pyenv`](https://github.com/pyenv/pyenv).

Follow [Installing and using pyenv](https://github.com/coopdevs/handbook/wiki/Installing-and-using-pyenv), or, in short:

```sh
pyenv install 3.8.12
pyenv virtualenv 3.8.12 community-maps-provisioning
```

### Configure ansible environment

You will need Ansible on your machine to run the playbooks, follow the steps below to install it.

```sh
pyenv exec pip install -r requirements.txt
ansible-galaxy install -r requirements.yml -f
```

### Install pre-commit hooks

We use [pre-commit framework](https://pre-commit.com/) to asure quality code.

```sh
pre-commit install
```

## Development

### Using lxc ( recommended for Linux )

* install devenv https://github.com/coopdevs/devenv
* run the following

```sh
cd /path/to/community-maps-provisioning
devenv
```

### Configure sysadmins users and execute provision

```sh
ansible-playbook playbooks/sys_admins.yml --limit=dev --user=root
ansible-playbook playbooks/provision.yml --limit=dev
```

## Test/Staging

You can find the vault pass in Coopdevs Bitwarden with the name:

`Community Maps test provisioning secret`

### Configure sysadmins users and execute provision

```sh
ansible-playbook playbooks/sys_admins.yml --limit=test --user=root --ask-vault-pass
ansible-playbook playbooks/provision.yml --limit=test --ask-vault-pass
```

### Deploy

```sh
ansible-playbook playbooks/deploy.yml --limit=test --ask-vault-pass
```
