ansible==5.3.0
ansible-lint==5.4.0
yamllint==1.26.3
pre-commit==2.17.0
